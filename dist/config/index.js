"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    "port": 4004,
    "mongoUrl": "mongodb://localhost:27017/product-api",
    "bodyLimit": "100kb"
};
//# sourceMappingURL=index.js.map