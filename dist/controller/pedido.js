'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _express = require('express');

var _pedido = require('../model/pedido');

var _pedido2 = _interopRequireDefault(_pedido);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
	var config = _ref.config,
	    db = _ref.db;

	var api = (0, _express.Router)();

	// POST
	api.post('/', function (req, res) {
		var newPedido = new _pedido2.default();
		newPedido.cliente = req.body.cliente;
		newPedido.data_emissao = new Date().toISOString();

		newPedido.save(function (err) {
			if (err) {
				res.send(err);
			}
			res.json({ message: 'Pedido foi salvo com sucesso' });
		});
	});

	// READ
	api.get('/:id', function (req, res) {
		_pedido2.default.findById(req.params.id, function (err, pedido) {
			if (err) {
				res.send(err);
			}
			res.json(pedido);
		});
	});

	// PUT
	api.put('/:id', function (req, res) {
		_pedido2.default.findById(req.params.id, function (err, pedido) {
			if (err) {
				res.send(err);
			}

			pedido.cliente = req.body.cliente;
			pedido.save(function (err) {
				if (err) {
					res.send(err);
				}
				res.json({ message: "Informação de Pedido Atualizada" });
			});
		});
	});

	// DELETE
	api.delete('/:id', function (req, res) {

		_pedido2.default.remove({
			_id: req.params.id
		}, function (err, pedido) {
			if (err) {
				res.send(err);
			}
			res.json({ message: "Pedido removido com sucesso!" });
		});
	});

	return api;
};
//# sourceMappingURL=pedido.js.map