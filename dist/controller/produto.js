'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _express = require('express');

var _produto = require('../model/produto');

var _produto2 = _interopRequireDefault(_produto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
	var config = _ref.config,
	    db = _ref.db;

	var api = (0, _express.Router)();

	// POST
	api.post('/', function (req, res) {
		var newProduto = new _produto2.default();
		newProduto.descricao = req.body.descricao;

		newProduto.save(function (err) {
			if (err) {
				res.send(err);
			}
			res.json({ message: 'Produto foi salvo com sucesso' });
		});
	});

	// READ
	api.get('/:id', function (req, res) {
		_produto2.default.findById(req.params.id, function (err, Produto) {
			if (err) {
				res.send(err);
			}
			res.json(Produto);
		});
	});

	// PUT
	api.put('/:id', function (req, res) {
		_produto2.default.findById(req.params.id, function (err, Produto) {
			if (err) {
				res.send(err);
			}

			Produto.cliente = req.body.descricao;
			Produto.save(function (err) {
				if (err) {
					res.send(err);
				}
				res.json({ message: "Informação de Produto Atualizada" });
			});
		});
	});

	// DELETE
	api.delete('/:id', function (req, res) {

		_produto2.default.remove({
			_id: req.params.id
		}, function (err, produto) {
			if (err) {
				res.send(err);
			}
			res.json({ message: "Produto removido com sucesso!" });
		});
	});

	return api;
};
//# sourceMappingURL=produto.js.map