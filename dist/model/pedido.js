'use strict';

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var pedidoSchema = new Schema({
	data_emissao: {
		type: String,
		default: new Date().toISOString(),
		required: true
	},
	cliente: {
		type: String,
		required: true
	},
	items: [{ type: Schema.Types.ObjectId, ref: 'Produtos' }]
});

module.exports = _mongoose2.default.model('Pedido', pedidoSchema);
//# sourceMappingURL=pedido.js.map