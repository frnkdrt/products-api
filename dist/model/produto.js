'use strict';

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _pedido = require('./pedido');

var _pedido2 = _interopRequireDefault(_pedido);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;

var ProdutoSchema = new Schema({
	descricao: {
		type: String,
		required: true
	},
	valor: {
		type: Number,
		required: true
	},
	pedido: {
		type: Schema.Types.ObjectId,
		ref: 'Pedido',
		required: true
	}
});

module.exports = _mongoose2.default.model('Produto', ProdutoSchema);
//# sourceMappingURL=produto.js.map