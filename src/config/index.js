export default {
    "port" : 4004,
    "mongoUrl" : "mongodb://localhost:27017/product-api",
    "bodyLimit" : "100kb"
}
