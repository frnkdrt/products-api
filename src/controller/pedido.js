import mongoose from 'mongoose'
import { Router } from 'express'
import Pedido from '../model/pedido'

export default  ({ config, db }) => {
	let api = Router()

	// POST
	api.post('/', (req, res) => {
		let newPedido = new Pedido()
		newPedido.cliente = req.body.cliente
		newPedido.data_emissao = new Date().toISOString()
		newPedido.items = req.body.items

		newPedido.save(err => {
			if (err) {
				res.send(err)
			}
			res.json({ message: 'Pedido foi salvo com sucesso'})
		})
	})

	// READ ALL
	api.get('/', (req, res) => {
		Pedido.find((err, pedidos) => {
			if (err) {
				res.send(err)
			}
			res.json(pedidos)
		})
	})

	// READ 
	api.get('/:id', (req, res) => {
		Pedido.findById(req.params.id, (err, pedido) => {
			if (err) {
				res.send(err)
			}
			const total = pedido.items.reduce((acc, item) => acc += item.valor, 0)

			res.json({ 
				pedido: pedido,
				total
			})
		})
	})

	// PUT
	api.put('/:id', (req, res) => {
		Pedido.findById(req.params.id, (err, pedido) => {
			if (err) {
				res.send(err)
			}

			pedido.cliente = req.body.cliente
			pedido.save(err => {
				if (err) {
					res.send(err)
				}
				res.json({ message: "Informação de Pedido Atualizada"})
			})
		})
	})

	// DELETE
	api.delete('/:id', (req, res) => {

		Pedido.remove({
			_id: req.params.id
		}, (err, pedido) => {
			if (err) {
				res.send(err)
			}
			res.json({ message: "Pedido removido com sucesso!"})
		})
	})

	return api
}

