import mongoose from 'mongoose'
import { Router } from 'express'
import Produto from '../model/produto'

export default  ({ config, db }) => {
	let api = Router()

	// POST
	api.post('/', (req, res) => {
		let newProduto = new Produto()
		newProduto.descricao = req.body.descricao

		newProduto.save(err => {
			if (err) {
				res.send(err)
			}
			res.json({ message: 'Produto foi salvo com sucesso'})
		})
	})

	// READ
	api.get('/:id', (req, res) => {
		Produto.findById(req.params.id, (err, Produto) => {
			if (err) {
				res.send(err)
			}
			res.json(Produto)
		})
	})

	// PUT
	api.put('/:id', (req, res) => {
		Produto.findById(req.params.id, (err, Produto) => {
			if (err) {
				res.send(err)
			}

			Produto.descricao = req.body.descricao
			Produto.save(err => {
				if (err) {
					res.send(err)
				}
				res.json({ message: "Informação de Produto Atualizada"})
			})
		})
	})

	// DELETE
	api.delete('/:id', (req, res) => {

		Produto.remove({
			_id: req.params.id
		}, (err, produto) => {
			if (err) {
				res.send(err)
			}
			res.json({ message: "Produto removido com sucesso!"})
		})
	})

	return api
}

