import mongoose from 'mongoose'

let Schema = mongoose.Schema

let pedidoSchema = new Schema({
	data_emissao : {
		type: String,
		default: new Date().toISOString()
	},
	cliente : {
		type: String,
		required: true
	},
	items: [{ produto: {type: Schema.Types.ObjectId, ref: 'Produtos'}, valor: Number }]
})

module.exports = mongoose.model('Pedido', pedidoSchema)