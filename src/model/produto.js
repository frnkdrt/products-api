import mongoose from 'mongoose'
import Pedido from './pedido'
let Schema = mongoose.Schema

let ProdutoSchema = new Schema({
	descricao: {
		type: String,
		required: true
	}
})

module.exports = mongoose.model('Produto', ProdutoSchema)