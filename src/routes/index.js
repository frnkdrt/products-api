import express from 'express';
import config from '../config';
import  middleware from '../middleware';
import initializeDb from '../db';

import pedido from '../controller/pedido.js'
import produto from '../controller/produto.js'

let router = express();


// connect to db
initializeDb(db => {
    // internal middleware
    router.use(middleware({ config, db }))

    // api routes v1 (/v1)
    router.use('/pedidos', pedido({ config, db}))
    router.use('/produtos', produto({ config, db}))
});

export default router;
